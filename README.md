# onoma-ontology



## Description

This ontology is designated to describe the concepts in the field on onomastics.

## History
This ontology is a continuation of the Onoma Ontology, started by Mr. Craig Trim, and described by him in the article [An Onomastic Compilation and Ontology](./resources/the-art-of-tokenization/index.md).
