# _An Onomastic Compilation and Ontology_, by Craig Trim

## Introduction

 There are many words in English that end with the suffix "-nym" or "-nymy". This comes from the ancient Greek ὄνυμα, meaning "name" or "word", and could even be loosely translated as "state of being".

I have searched for how often each type of onoma is mentioned in this open-domain corpus. Note that I have not searched for synonyms themselves, and how often a word (being the instance of set of synonyms) occurs, but how often the class type itself occurs.

This is by far an easier method of perhaps gaining some idea of how common each type of onoma is. An indeed, the first few entries on the list would seem to bear this out.

However, this method is not entirely accurate. The use of "backronym" is quite infrequent, perhaps due to its more common and recent use in internet related corpora, rather than printed books. This is my anecdotal view. Some words like "retronym" are more likely to occur in internet corpora, and are by definition then, more recent. Many definitions came from Wiktionary. Some from the OED. Provenance is maintained throughout the Ontology.

In many respects, the class of words ending with "-nym" could be considered open. Neologism (the type of words belonging to the class "neonym") can be easily created. This is both a strength and weakness of English. The easy ability to combine with other languages and form new words has kept English a viable lingua franca, but can cause confusion as well as semantic overlap with other neologizers – 

> Among all other lessons this should first be learned, that wee never affect any straunge ynkehorne termes, but to speake as is commonly received ...
>
> Some seeke so far for outlandish English, that they forget altogether their mothers language. And I dare sweare this, if some of their mothers were alive, thei were not able to tell what they say ...  
>
> The simple can not but wonder at their talke, and thinke surely they speake by some revelation. I know them that thinke Rhetorique to stande wholie upon darke wordes, and hee that can catche an ynke horne terme by the taile, him they coumpt to be a fine Englisheman, and a good Rhetorician.
>
> ##### Cable, Thomas (2007-03-20). A History of the English Language, Fifth Edition (p. 203). Taylor & Francis. Kindle Edition. Quoting Thomas Wilson, whose Arte of Rhetorique (1553)

However, in the desire to create a consistent hierarchy for the onoma, I have (almost be necessity) been impelled to coin onoma, hoping not to cause mere inflation of terms, but lend itself to simplicity and ability to refer to a class of onoma by the induced hypernym.

## Relevance to Semantic Modeling

 A categorization of onomastic terminology is a helpful step in understanding data. In the automated creation of a semantic model, it is necessary to develop patterns. Semantic models are primarily composed of space (static information) and time (process / event oriented). Patterns built around onoma help is deriving the former.

It is quite common to gather a list of synonyms and acronyms for a domain; this can be considered a more comprehensive list of patterns and word types.

## The Onoma Ontology

Most of these relationships are used for each Onoma instance (let this represent the schema):

```turtle
    onoma:Name
        rdfs:subClassOf onoma:Name ;
        rdfs:label value@us^^xsd:string ;
        rdfs:prefix prefix@gr^^xsd:string ;
        rdfs:suffix "onumon"@us^^xsd:string ;
        rdfs:translation value@us^^xsd:string ;

        owl:equivalentClass onoma:Name ;
        owl:disjointWith onoma:Name ;

        dc:coverage number^^xsd:integer ;
        dc:date number^^xsd:integer ;

        skos:narrower onoma:Name ;
        skos:broader onoma:Name ;
        skos:related onoma:Name ;

        prov:wasAttributedTo :entity ;
        prov:hadPrimarySource :entity ;
        prov:wasQuotedFrom :entity ;
        prov:wasDerivedFrom :entity ;
        prov:wasGeneratedBy :activity .
```

## Schema Instance

```rdf
    onoma:Allonym
        rdf:type owl:Class ;
        rdfs:definition "The name of a person, usually a historical person, assumed by a writer."@us , "A variant of a name stemming from the same etymological root."@us , "A work published under a name that is not that of the author."@us ;
        rdfs:label "Allonym"@us ;
        rdfs:prefix "allos"@fr ;
        rdfs:subClassOf onoma:Pseudonym ;
        rdfs:suffix "onumon"@gr ;
        rdfs:translation "other name"@us ;
        dc:coverage "34400"^^xsd:string , "159"^^xsd:string ;
        dc:date "1873"^^xsd:string ;
        skos:broader onoma:Anonym .
```
## Use of Dublin Core

_dc:coverage_

The Google n-Grams viewer is a great etymological tool for retrieving the first mention of a term.  The returned data is (unfortunately) context free, but on the positive side, often delivers results where the Oxford English Dictionary fails to deliver.  The use of this Dublin Core property gives first mention via this query:

```
http://books.google.com/ngrams/graph?content=<onoma>&year_start=1500&year_end=2000
```

_dc:date_

First mention of this onoma in

```
The Google Unigrams Corpus (optional)

Other Sources (optional)
```

## Use of SKOS

_skos:narrower, broader, related_

A Synonym of narrower meaning, broader meaning, or a synonym that is not categorized as either broader nor narrower.

## Use of PROV

As the Ontology is primarily a work of compilation, extensive use is made of the [PROV](http://trimc-nlp.blogspot.com/2013/05/w3c-provenance-model-primer.html) Ontology.

_[prov:wasAttributedTo](http://w3.org/TR/prov-o/#wasAttributedTo)_

Attribution is the ascribing of an entity to an agent.

_[prov:hasPrimarySource](http://w3.org/TR/prov-o/#hadPrimarySource)_

A primary source for a topic refers to something produced by some agent with direct experience and knowledge about the topic.

_[prov:wasQuotedFrom](http://w3.org/TR/prov-o/#wasQuotedFrom)_

A quotation is the repeat of (some or all of) an entity, such as text or image, by someone who may or may not be its original author.

_[prov:wasDerivedFrom](http://w3.org/TR/prov-o/#wasDerivedFrom)_

A derivation is a transformation of an entity into another.

_[prov:wasGeneratedBy](http://w3.org/TR/prov-o/#generated)_
Generation is the completion of production of a new entity by an activity. 

Posted 9th July 2013 by Craig Trim

Labels: dublin core Linguistics Natural Language Processing onoma onomatopoeia Ontology Modeling prov semantic modeling Semantic Web skos
