@prefix : <https://kuberam.ro/ontologies/text-index#> .
@prefix rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#> .
@prefix owl: <http://www.w3.org/2002/07/owl#> .
@prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .
@prefix xsd: <http://www.w3.org/2001/XMLSchema#> .
@prefix dct: <http://purl.org/dc/terms/> .
@prefix bibo: <http://purl.org/ontology/bibo/> .
@prefix sw: <http://www.w3.org/2003/06/sw-vocab-status/ns#> .
@prefix vann: <http://purl.org/vocab/vann/> .
@prefix foaf: <http://xmlns.com/foaf/0.1/> .

<https://kuberam.ro/ontologies/text-index> rdf:type owl:Ontology ;
    vann:preferredNamespaceUri "https://kuberam.ro/ontologies/onoma" ;
    vann:preferredNamespacePrefix "onoma"@en ;
    rdfs:label "Onomastics"@en ;
    dct:title "The onomastics ontology"@en ;
    dct:description """Ontology for the onomastics domain. It provides description of the concepts in this domain."""@en ;
    dct:abstract "This ontology is designated to present and describe the concepts that are specific to the ontology domain."@en ;
    bibo:status <http://purl.org/ontology/bibo/status/published> ;
    owl:versionIRI <https://kuberam.ro/ontologies/text-index/1.0.0> ;
    owl:versionInfo "1.0.0"@en ;
    # owl:backwardCompatibleWith <https://kuberam.ro/ontologies/text-index/1.0.0> ;
    # owl:incompatibleWith <https://kuberam.ro/ontologies/text-index/0.0.1> ;
    # owl:priorVersion <https://kuberam.ro/ontologies/text-index/1.0.0> ;
    dct:created "2024-06-04"^^xsd:date ;
    # dct:modified "2024-02-02"^^xsd:date 
    dct:creator <https://orcid.org/0009-0009-7237-1824> ;
    # bibo:doi "10.5281/zenodo.6940891" .
    dct:bibliographicCitation "Cite this ontology as: Teodorescu, C. M. Onoma ontology 1.0.0."@en ;
    dct:license <http://creativecommons.org/licenses/by/4.0/> ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

<https://orcid.org/0009-0009-7237-1824> foaf:name "Claudius Marian Teodorescu"@en ;
	org:memberOf <https://unitbv.ro#this> .

<https://unitbv.ro#this> rdf:type owl:NamedIndividual , foaf:Organization ;
    foaf:homepage "https://unitbv.ro" ;
    foaf:name "Transilvania University of Brașov, Romania"@en .

#################################################################
#    Annotation properties
#################################################################

###  http://purl.org/dc/elements/1.1/abstract
<http://purl.org/dc/elements/1.1/abstract> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/created
<http://purl.org/dc/elements/1.1/created> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/description
<http://purl.org/dc/elements/1.1/description> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/publisher
<http://purl.org/dc/elements/1.1/publisher> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/elements/1.1/title
<http://purl.org/dc/elements/1.1/title> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/terms/created
<http://purl.org/dc/terms/created> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/terms/creator
<http://purl.org/dc/terms/creator> rdf:type owl:AnnotationProperty .


###  http://purl.org/dc/terms/license
<http://purl.org/dc/terms/license> rdf:type owl:AnnotationProperty .


###  http://purl.org/vocab/vann/example
<http://purl.org/vocab/vann/example> rdf:type owl:AnnotationProperty .


###  http://purl.org/vocab/vann/preferredNamespacePrefix
<http://purl.org/vocab/vann/preferredNamespacePrefix> rdf:type owl:AnnotationProperty .


###  http://purl.org/vocab/vann/preferredNamespaceUri
<http://purl.org/vocab/vann/preferredNamespaceUri> rdf:type owl:AnnotationProperty .


###  http://schema.org/author
<http://schema.org/author> rdf:type owl:AnnotationProperty .


###  http://schema.org/citation
<http://schema.org/citation> rdf:type owl:AnnotationProperty .


#################################################################
#    Datatypes
#################################################################

###  http://www.w3.org/2001/XMLSchema#string
xsd:string rdf:type rdfs:Datatype .

###  http://www.w3.org/2001/XMLSchema#anyURI
xsd:anyURI rdf:type rdfs:Datatype .

###  http://www.w3.org/2001/XMLSchema#date
xsd:date rdf:type rdfs:Datatype .


#################################################################
#    Object Properties
#################################################################

###  https://kuberam.ro/ontologies/text-index#type
:type rdf:type owl:ObjectProperty ;
    rdfs:domain :Index ;
    rdfs:range :Index ;
    rdfs:comment "The type of the index."@en ;
    rdfs:label "index type"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#metadataFor
:metadataFor rdf:type owl:ObjectProperty ;
    rdfs:domain :IndexMetadata ;
    rdfs:range :Index ;
    rdfs:comment "The index the metadata is for."@en ;
    rdfs:label "metadata for"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .


#################################################################
#    Data properties
#################################################################

###  https://kuberam.ro/ontologies/text-index#title
:title rdf:type owl:DatatypeProperty ;
    rdfs:domain :Index ;
    rdfs:range xsd:string ;
    rdfs:comment "The title of the index."@en ;
    rdfs:label "title"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#isDefaultIndex
:isDefaultIndex rdf:type owl:DatatypeProperty ;
    rdfs:domain :Index ;
    rdfs:range xsd:boolean ;
    rdfs:comment "If the current index is the default one in the search interface."@en ;
    rdfs:label "is default index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#textBaseIRI
:textBaseIRI rdf:type owl:DatatypeProperty ;
    rdfs:domain :Index ;
    rdfs:range xsd:anyURI ;
    rdfs:comment "The base IRI for the text the index is for. The text can be a document, or more documents."@en ;
    rdfs:label "text base IRI"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#selector
:selector rdf:type owl:DatatypeProperty ;
    rdfs:domain :Index ;
    rdfs:range xsd:string ;
    rdfs:comment "The selector for the token to be included in the index. This is to be used for XML documents."@en ;
    rdfs:label "selector"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#format
:format rdf:type owl:DatatypeProperty ;
    rdfs:domain :Index ;
    rdfs:range xsd:string ;
    rdfs:comment "The binary format of the index. The initial format is text (RDF Turtle). For searching, the index has to be converted in a binary format."@en ;
    rdfs:label "format"@en ;
    <http://purl.org/vocab/vann/example> """
        Format s_prefix_regex_levenstein, for searching by prefix, by regular expression, and by levenstein distance, respectively.
    """@en ;    
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .
 

###  https://kuberam.ro/ontologies/text-index#headword
:headword rdf:type owl:DatatypeProperty ;
    rdfs:domain :HeadwordsIndex ;
    rdfs:range xsd:string ;
    rdfs:comment "The headword a text segment contains."@en ;
    rdfs:label "has headword"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#word
:word rdf:type owl:DatatypeProperty ;
    rdfs:domain :WordsIndex ;
    rdfs:range xsd:string ;
    rdfs:comment "The word a text segment contains."@en ;
    rdfs:label "has word"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#keyword
:keyword rdf:type owl:DatatypeProperty ;
    rdfs:domain :KeywordsIndex ;
    rdfs:range xsd:string ;
    rdfs:comment "The keyword a text segment contains."@en ;
    rdfs:label "has keyword"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#date
:date rdf:type owl:DatatypeProperty ;
    rdfs:domain :DatesIndex ;
    rdfs:range xsd:string ;
    rdfs:comment "The date a text segment contains."@en ;
    rdfs:label "has date"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#allCharacters
:allCharacters rdf:type owl:DatatypeProperty ;
    rdfs:domain :IndexMetadata ;
    rdfs:range xsd:string ;
    rdfs:comment "The distinct all characters of all the headwords of an index."@en ;
    rdfs:label "all characters" ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#characterNormalisationMapping
:characterNormalisationMapping rdf:type owl:DatatypeProperty ;
    rdfs:domain :IndexMetadata ;
    rdfs:range xsd:string ;
    rdfs:comment "A mapping of a character to another character, tipically from a character with diacritic to a character without diacritic, needed for normalisation of characters. The syntax is `ú=u`. This is used for composing regular expressions used for searching."@en ;
    rdfs:label "character normalization mapping" ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#metadataConstructQuery
:metadataConstructQuery rdf:type owl:DatatypeProperty ;
    rdfs:domain :IndexMetadata ;
    rdfs:range xsd:string ;
    rdfs:comment "A SPARQL query used for construction of new metadata, by using any ontology that is needed."@en ;
    rdfs:label "metadata construct query" ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .
    
###  https://kuberam.ro/ontologies/text-index#searchFacet
:searchFacet rdf:type owl:DatatypeProperty ;
    rdfs:domain :IndexMetadata ;
    rdfs:comment "A search facet, used to group indexed headwords in categories."@en ;
    rdfs:label "search facet" ;
    <http://purl.org/vocab/vann/example> """
        @prefix schema: <https://schema.org/> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

        <metadata.ttl> i:searchFacet _:b1 .
        _:b1 rdfs:label "a" .
        _:b1 schema:valuePattern "a.*" .
    """@en ;
    <http://purl.org/vocab/vann/example> """
        @prefix schema: <https://schema.org/> .
        @prefix rdfs: <http://www.w3.org/2000/01/rdf-schema#> .

        <metadata.ttl> i:searchFacet _:b2 .
        _:b2 rdfs:label "adjective"@en .
        _:b2 rdfs:label "adjectif"@fr .
        _:b2 rdfs:label "adjectiv"@ro .
        _:b2 schema:valuePattern "adj\." .
    """@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

#################################################################
#    Classes
#################################################################

###  https://kuberam.ro/ontologies/text-index#Index
:Index rdf:type owl:Class ;
    <http://purl.org/vocab/vann/example> "Full Text Index"@en ;
    rdfs:comment "A list of words with reference to the text segments the words are part of."@en ;
    rdfs:label "Index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#IndexMetadata
:IndexMetadata rdf:type owl:Class ;
    <http://purl.org/vocab/vann/example> "Metadata for an index of dictionary headwords"@en ;
    rdfs:comment "metadata for and index of all the headwords of a dictionary."@en ;
    rdfs:label "Index metadata"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .    

###  https://kuberam.ro/ontologies/text-index#HeadwordsIndex
:HeadwordsIndex rdf:type owl:Class ;
    rdfs:subClassOf :Index ;
    <http://purl.org/vocab/vann/example> "Index of dictionary headwords"@en ;
    rdfs:comment "index of all the headwords of a dictionary."@en ;
    rdfs:label "Headwords index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#WordsIndex
:WordsIndex rdf:type owl:Class ;
    rdfs:subClassOf :Index ;
    <http://purl.org/vocab/vann/example> "Index of all the words"@en ;
    rdfs:comment "index of all the words of a text."@en ;
    rdfs:label "Words index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#KeywordsIndex
:KeywordsIndex rdf:type owl:Class ;
    rdfs:subClassOf :Index ;
    <http://purl.org/vocab/vann/example> "Index of keywords"@en ;
    rdfs:comment "index of all the keywords of a text."@en ;
    rdfs:label "Keywords index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

###  https://kuberam.ro/ontologies/text-index#DatesIndex
:DatesIndex rdf:type owl:Class ;
    rdfs:subClassOf :Index ;
    <http://purl.org/vocab/vann/example> "Index of dates"@en ;
    rdfs:comment "index of all the dates of a text."@en ;
    rdfs:label "Dates index"@en ;
    sw:term_status <http://purl.org/linked-data/registry#statusValid> .

[] rdf:type owl:AllDisjointClasses ;
    owl:members  ( :HeadwordsIndex  :WordsIndex :KeywordsIndex ) .


### TODO:
# metadata: minimal number of characters to trigger the autocompleter
# maybe one property for any type of index, namely i:h, instead of i:date, i:word, i:headword, i:keyword?
# Number of main entries of an index.

### SYNTAX FOR COMMANDING A FULL TEXT INDEX
# fulltext (title,body) with parser ngram
# https://jena.apache.org/documentation/query/text-query.html#text-dataset-assembler
